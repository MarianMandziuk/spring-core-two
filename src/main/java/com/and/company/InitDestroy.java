package com.and.company;

public interface InitDestroy {
    default void customInitMethod() {
        System.out.println("custom init method of class " + getClass().getSimpleName());
    }

    default void customDestroyMethod() {
        System.out.println("custom destroy method of class " + getClass().getSimpleName());
    }
}
