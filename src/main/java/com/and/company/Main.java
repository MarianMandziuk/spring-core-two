package com.and.company;

import com.and.company.config.ConfigOne;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext ctx =
            new AnnotationConfigApplicationContext(ConfigOne.class);

        ctx.close();
    }
}
