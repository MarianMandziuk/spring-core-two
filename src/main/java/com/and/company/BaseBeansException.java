package com.and.company;

import org.springframework.beans.BeansException;

public class BaseBeansException extends BeansException {

    public BaseBeansException(String message) {
        super(message);
    }

    public BaseBeansException(String message, Throwable cause) {
        super(message, cause);
    }
}
