package com.and.company.beans;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class BeanE extends BaseBean {
    public BeanE(String name, int value) {
        super(name, value);
    }

    @PostConstruct
    public void postConstruct() {
        System.out.println("post construct method of class " + getClass().getSimpleName());
    }

    @PreDestroy
    public void preDestroy() {
        System.out.println("pre destroy method of class " + getClass().getSimpleName());
    }
}
