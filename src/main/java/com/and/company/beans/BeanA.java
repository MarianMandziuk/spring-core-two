package com.and.company.beans;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class BeanA extends BaseBean implements InitializingBean, DisposableBean {
    public BeanA(String name, int value) {
        super(name, value);
    }

    @Override
    public void destroy() throws Exception {
        System.out.println("destroy method of class " + getClass().getSimpleName());
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("after properties set method of class " + getClass().getSimpleName());
    }
}
