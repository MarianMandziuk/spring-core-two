package com.and.company.beans;

public abstract class BaseBean {
    protected String name;
    protected int value;

    public BaseBean(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "name='" + name + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
