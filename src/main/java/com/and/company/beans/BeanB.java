package com.and.company.beans;

import com.and.company.InitDestroy;

public class BeanB extends BaseBean implements InitDestroy {
    public BeanB(String name, int value) {
        super(name, value);
    }

    public void otherInitMethod() {
        System.out.println("other init method of class " + getClass().getSimpleName());
    }
}
