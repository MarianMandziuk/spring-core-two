package com.and.company.beans;

import com.and.company.BaseBeanValidator;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class BeanPP implements BeanPostProcessor {

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("post proccess after initialization of class " + getClass().getSimpleName());
        System.out.println("validation of class " + bean.getClass().getSimpleName());
        if (bean instanceof BaseBean) {
            BaseBeanValidator.validate((BaseBean) bean);
        }
        return bean;
    }
}
