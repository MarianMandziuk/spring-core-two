package com.and.company.beans;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

public class BeanFPP implements BeanFactoryPostProcessor {
    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        System.out.println("post process bean factory method of class " + getClass().getSimpleName());
        BeanDefinition beanDefinition = beanFactory.getBeanDefinition("beanB");
        beanDefinition.setInitMethodName("otherInitMethod");
        System.out.println("post process bean factory changed beanB init method");
    }
}
