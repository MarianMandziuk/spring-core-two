package com.and.company;

import com.and.company.beans.BaseBean;
import org.springframework.beans.BeansException;

public class BaseBeanValidator {
    public static void validate(BaseBean bean) throws BeansException {
        validateNull(bean);
        validateNegetive(bean);
    }

    private static void validateNull(BaseBean bean) throws BeansException {
        if (bean.getName() == null)
            throw new BaseBeansException("Name can't be null.");
    }

    private static void validateNegetive(BaseBean bean) throws BeansException {
        if (bean.getValue() < 0)
            throw new BaseBeansException("Value can't be negetive.");
    }
}
