package com.and.company.config;

import com.and.company.beans.BeanB;
import com.and.company.beans.BeanC;
import com.and.company.beans.BeanD;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("application.properties")
public class ConfigTwo {
    @Value("${beanb.name}")
    private String nameB;
    @Value("${beanb.value}")
    private int valueB;

    @Value("${beanc.name}")
    private String nameC;
    @Value("${beanc.value}")
    private int valueC;

    @Value("${beand.name}")
    private String nameD;
    @Value("${beand.value}")
    private int valueD;

    @Bean(name = "beanB",
          initMethod = "customInitMethod",
          destroyMethod = "customDestroyMethod")
    @DependsOn("beanD")
    public BeanB getBeanB() {
        return new BeanB(nameB, valueB);
    }

    @Bean(name = "beanC",
          initMethod = "customInitMethod",
          destroyMethod = "customDestroyMethod")
    @DependsOn("beanB")
    public BeanC getBeanC() {
        return new BeanC(nameC, valueC);
    }

    @Bean(name = "beanD",
          initMethod = "customInitMethod",
          destroyMethod = "customDestroyMethod")
    public BeanD getBeanD() {
        return new BeanD(nameD, valueD);
    }

}
