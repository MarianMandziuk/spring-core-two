package com.and.company.config;

import com.and.company.beans.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Lazy;

@Configuration
@Import(ConfigTwo.class)
public class ConfigOne {

    @Bean(name = "beanF")
    @Lazy
    public BeanF getBeanF() {
        return new BeanF("bean f", 6);
    }

    @Bean(name = "beanA")
    public BeanA getBeanA() {
        return new BeanA("bean a", 1);
    }

    @Bean(name = "beanE")
    public BeanE getBeanE() {
        return new BeanE("bean e", 5);
    }

    @Bean(name = "beanFPP")
    public static BeanFPP getBeanFPP() {
        return new BeanFPP();
    }

    @Bean(name = "beanPP")
    public BeanPP getBeanPP() {
        return new BeanPP();
    }
}
